import gnupg, urllib2, argparse, re, copy
from bs4 import BeautifulSoup

signs = {}
names = []
labels = {}

def getKeys(key, depth, sigs = {}):
  global signs, deep
  if "0x" in key:
    key = "0x" + key[2:].upper()
  else:
    key = "0x" + key.upper()

  url = "http://key.bbs4.us/pks/lookup?op=vindex&search=" + key
  soup = BeautifulSoup(urllib2.urlopen(url).read())
  body = soup.body

  if "revok" not in str(body):
    name = str(body).split("</span>")[0].split('uid">')[1].split("&lt")[0][:-1]
    if name not in names:
      names.append(name)
      labels[key[-8:]] = name

    lines = re.split("\r\n", str(body))

    uidlines = [s for s in lines if "uid" in s]
    for index, uidline in enumerate(uidlines):
      uidlines[index] = lines.index(uidline)
    
    for index, line in enumerate(lines):
      if index > uidlines[0] or (len(uidlines) > 1 and index < uidlines[1]):
        if "selfsig" not in line and "sig  sig   " in line:
          s = line.split("______")[2].split(">")[0].split("h=")[1][:-1][-8:]
          if key[-8:] not in signs:
            if depth == 1:
              signs[key[-8:]] = ["flip" + s]
            else:
              signs[key[-8:]] = [s]
          else:
            signs[key[-8:]].append(s)
            sign = s + " -> " + key[-8:]
          if s not in sigs:
            sigs[s] = "O"

    for sig in copy.deepcopy(sigs):
      if sigs[sig] != "X" and depth+1 <= deep:
        sigs[sig] = "X"
        getKeys(sig, depth+1, sigs)

parser = argparse.ArgumentParser(description='Visualize a web of trust')
parser.add_argument('--remote', help='str: Last 2 hexadecimals', nargs='*')
parser.add_argument('--depth', help='int: How far you wanna go out', nargs='*')

args = parser.parse_args()
remote = False
if args.remote is not None: remote = args.remote

deep = 100
if args.depth is not None: deep = args.depth

if remote != False:
  if len(remote[0]) == 8 :
    getKeys("0x" + remote[0], 1)
  elif (len(remote[0])) == 10 and remote[0][:2] == "0x":
    getKeys(remote[0], 1)
  else:
    print "The remote argument is the last 2 hexadecimals of your fingerprint"
    print "For example: $ python grafsta.py --remote 5A542B6C"
elif not remote:
  gpg = gnupg.GPG()
  gpg.encoding = "utf-8"

  pubs = gpg.list_keys()
  sigs = {}

  for pub in pubs:
    search = gpg.search_keys("0x" + pub["keyid"][-8:], "key.bbs4.us/")
    if len(search) > 0:
      sigs["0x" + pub["keyid"][-8:]] = "O"

  for sig in copy.deepcopy(sigs):
    getKeys(sig, 1, sigs)

with open("web.dot", "a") as myfile:
  myfile.write('digraph "web" {\n  overlap=scale\n  splines=true\n  sep=.1\n\n')
  myfile.write("\n")
  for index, label in labels.iteritems():
    myfile.write('  "' + index + '"\n  [label="' + label + '"]\n')
  myfile.write("\n")
  for index, sign in signs.iteritems():
    string = ""
    for s in sign:
      string += s + ", "
    if "flip" in string:
      myfile.write("  " + index + " -> { " + string[4:-2] + " }\n")
    else:
      myfile.write("  { " + string[:-2] + " } -> " + index + "\n")
  myfile.write("}")